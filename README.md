## Quran Player

Create beautiful UI to play Quran stored in the "quran folder" using the HTML5 audio API

## Project Specifications

- Create UI for Quran player including spinning image and Surah(chapter) detail popup
- Add play and pause functionality
- Switch chapters
- Progress bar

[quranPlayerWebsite](https://quran-player-saadmu7ammad-b11187a0b3052e679ffe3a6c6d63565faf744.gitlab.io/)